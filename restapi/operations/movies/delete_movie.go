// Code generated by go-swagger; DO NOT EDIT.

package movies

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// DeleteMovieHandlerFunc turns a function with the right signature into a delete movie handler
type DeleteMovieHandlerFunc func(DeleteMovieParams, interface{}) middleware.Responder

// Handle executing the request and returning a response
func (fn DeleteMovieHandlerFunc) Handle(params DeleteMovieParams, principal interface{}) middleware.Responder {
	return fn(params, principal)
}

// DeleteMovieHandler interface for that can handle valid delete movie params
type DeleteMovieHandler interface {
	Handle(DeleteMovieParams, interface{}) middleware.Responder
}

// NewDeleteMovie creates a new http.Handler for the delete movie operation
func NewDeleteMovie(ctx *middleware.Context, handler DeleteMovieHandler) *DeleteMovie {
	return &DeleteMovie{Context: ctx, Handler: handler}
}

/*DeleteMovie swagger:route DELETE /movies/{movie_id} movies deleteMovie

DeleteMovie delete movie API

*/
type DeleteMovie struct {
	Context *middleware.Context
	Handler DeleteMovieHandler
}

func (o *DeleteMovie) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewDeleteMovieParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal interface{}
	if uprinc != nil {
		principal = uprinc
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
