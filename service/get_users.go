package service

import (
	"admin-gateway/models"
	pbPayment "admin-gateway/proto/payment"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/users"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// CreateUser handles create user logic.
func (g *Gateway) GetUsers(params users.GetUsersParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return users.NewGetUsersUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return users.NewGetUsersForbidden()
	}
	gu, err := g.UserClient.GetUsers(params.HTTPRequest.Context(), &pbUser.UsersRequest{
		Limit:     params.Limit,
		Offset:    swag.Int32Value(params.Offset),
		Role:      pbUser.UserRole(swag.Int32Value(params.Role)),
		Status:    pbUser.UserStatus(swag.Int32Value(params.Status)),
		BirthDate: swag.Int64Value(params.BirthDate),
	})
	if err != nil {
		log.Printf(getUsersFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
		return users.NewGetUsersDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	gup := gu.GetPayload()
	mu := models.Users{Total: gu.GetTotal(), Payload: make([]*models.User, 0, len(gup))}
	for _, u := range gup {
		br, err := g.PaymentClient.GetUserBalance(params.HTTPRequest.Context(), &pbPayment.GetUserBalanceRequest{UserId: u.GetId()})
		if err != nil {
			log.Printf(getUserBalanceFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
			return users.NewGetUsersDefault(internalCode).WithPayload(wrapGRPCError(err))
		}
		mu.Payload = append(mu.Payload, &models.User{
			Balance:     float64(br.Balance),
			BirthDate:   u.GetBirthDate(),
			CreatedAt:   u.GetCreatedAt(),
			DisplayName: u.GetDispayName(),
			Email:       strfmt.Email(u.GetEmail()),
			ID:          u.GetId(),
			PhoneNumber: u.GetPhoneNumber(),
			Role:        int32(u.GetRole()),
			Status:      int32(u.GetStatus()),
			UpdatedAt:   u.GetUpdatedAt(),
		})
	}
	return users.NewGetUsersOK().WithPayload(&mu)
}
