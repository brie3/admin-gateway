package service

import (
	"admin-gateway/models"
	pbAuth "admin-gateway/proto/auth"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/users"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// UpdateUser handles update user logic.
func (g *Gateway) UpdateUser(params users.UpdateUserParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return users.NewUpdateUserUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return users.NewUpdateUserForbidden()
	}
	pr, err := g.AuthClient.PwdHash(params.HTTPRequest.Context(), &pbAuth.PwdRequest{Pwd: params.Body.Password})
	if err != nil {
		log.Printf(createPwdFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewUpdateUserDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	ur, err := g.UserClient.UpdateUser(params.HTTPRequest.Context(), &pbUser.UpdateUserRequest{
		Id:          params.UserID,
		Email:       params.Body.Email.String(),
		PwdHash:     pr.GetPwdHash(),
		Role:        pbUser.UserRole(params.Body.Role),
		Status:      pbUser.UserStatus(params.Body.Status),
		DispayName:  params.Body.DisplayName,
		PhoneNumber: params.Body.PhoneNumber,
		BirthDate:   params.Body.BirthDate,
	})
	if err != nil {
		log.Printf(updateUserFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewUpdateUserDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mu := models.User{}
	if err = swag.DynamicJSONToStruct(&ur, &mu); err != nil {
		log.Printf(jsonConvertOnUpdateUserFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewUpdateUserDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return users.NewUpdateUserOK().WithPayload(&mu)
}
