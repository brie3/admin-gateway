package service

import (
	pbMovie "admin-gateway/proto/movie"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/movies"
	"log"

	"github.com/go-openapi/runtime/middleware"
)

// DeleteMovie handles movie delettion logic.
func (g *Gateway) DeleteMovie(params movies.DeleteMovieParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return movies.NewDeleteMovieUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return movies.NewDeleteMovieForbidden()
	}
	_, err := g.MovieClient.DeleteMovie(params.HTTPRequest.Context(), &pbMovie.DeleteMovieRequest{Id: params.MovieID})
	if err != nil {
		log.Printf(deleteMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
		return movies.NewDeleteMovieDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewDeleteMovieNoContent()
}
