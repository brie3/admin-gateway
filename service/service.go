package service

import (
	"admin-gateway/models"
	"admin-gateway/proto/auth"
	"admin-gateway/proto/movie"
	"admin-gateway/proto/payment"
	"admin-gateway/proto/user"
	"log"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	badRequestCode                 = 400
	unauthorizedCode               = 401
	internalCode                   = 500
	internalFormat                 = "internal error"
	unauthorizeFormat              = "[%s] authorizing addr: %q"
	adminValidationFormat          = "[%s] admin role validation for addr: %q and email: %q"
	movieFormat                    = "[%s] get movie for addr: %q and email: %q got error: %v"
	moviesFormat                   = "[%s] get movies for addr: %q and email: %q got error: %v"
	loginFormat                    = "[%s] login for addr: %q; email: %q got error: %v"
	registerFormat                 = "[%s] register for addr: %q; email: %q got error: %v"
	createUserFormat               = "[%s] create user for addr: %q and email: %q for user email: %q got error: %v"
	createPwdFormat                = "[%s] create pwdHash for addr: %q and email: %q for user email: %q got error: %v"
	createMovieFormat              = "[%s] create movie for addr: %q and email: %q for movie title: %q got error: %v"
	updateUserFormat               = "[%s] update user for addr: %q and email: %q for user email: %q got error: %v"
	updateMovieFormat              = "[%s] update movie for addr: %q and email: %q for movie title: %q got error: %v"
	getUsersFormat                 = "[%s] get users for addr: %q and email: %q got error: %v"
	getUserBalanceFormat           = "[%s] get user balance for addr: %q and email: %q got error: %v"
	deleteMovieFormat              = "[%s] delete movie for addr: %q and email: %q got error: %v"
	deleteUserFormat               = "[%s] delete user for addr: %q and email: %q got error: %v"
	jsonConvertOnGetMovieFormat    = "[%s] json convert on get movie response for addr: %q got error: %v"
	jsonConvertOnGetMoviesFormat   = "[%s] json convert on get movies response for addr: %q got error: %v"
	jsonConvertOnUpdateMovieFormat = "[%s] json convert on update movie response for addr: %q and email: %q for movie title: %q got error: %v"
	jsonConvertOnCreateMovieFormat = "[%s] json convert on create user response for addr: %q and email: %q for movie title: %q got error: %v"
	jsonConvertOnCreateUserFormat  = "[%s] json convert on create user response for addr: %q and email: %q for user email: %q got error: %v"
	jsonConvertOnUpdateUserFormat  = "[%s] json convert on update user response for addr: %q and email: %q for user email: %q got error: %v"
)

// RequestIDContextKey handles uuid for requests.
var RequestIDContextKey = struct{}{}

// Gateway handles proto interactions.
type Gateway struct {
	UserClient                                 user.UserServiceClient
	MovieClient                                movie.MovieServiceClient
	AuthClient                                 auth.AuthServiceClient
	PaymentClient                              payment.PaymentServiceClient
	userConn, paymentConn, movieConn, authConn *grpc.ClientConn
}

func New() *Gateway {
	var (
		addr string
		ok   bool
		err  error
	)
	gate := Gateway{}
	addr, ok = os.LookupEnv("USER_SERVICE_ADDR")
	if !ok {
		log.Fatalf("USER_SERVICE_ADDR env is not set")
	}
	gate.userConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to user service: %v", err)
	}

	addr, ok = os.LookupEnv("PAYMENT_SERVICE_ADDR")
	if !ok {
		log.Fatalf("PAYMENT_SERVICE_ADDR env is not set")
	}
	gate.paymentConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to payment service: %v", err)
	}

	addr, ok = os.LookupEnv("MOVIE_SERVICE_ADDR")
	if !ok {
		log.Fatalf("MOVIE_SERVICE_ADDR env is not set")
	}
	gate.movieConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to movie service: %v", err)
	}

	addr, ok = os.LookupEnv("AUTH_SERVICE_ADDR")
	if !ok {
		log.Fatalf("AUTH_SERVICE_ADDR env is not set")
	}
	gate.authConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to auth service: %v", err)
	}

	gate.MovieClient = movie.NewMovieServiceClient(gate.movieConn)
	gate.AuthClient = auth.NewAuthServiceClient(gate.authConn)
	gate.UserClient = user.NewUserServiceClient(gate.userConn)
	gate.PaymentClient = payment.NewPaymentServiceClient(gate.paymentConn)
	return &gate
}

func (g *Gateway) Close() {
	log.Println(g.authConn.Close(), g.movieConn.Close(), g.userConn.Close(), g.paymentConn.Close())
}

func wrapGRPCError(err error) *models.Error {
	switch status.Code(err) {
	case codes.PermissionDenied:
		return &models.Error{Code: unauthorizedCode, Message: status.Convert(err).Message()}
	case codes.AlreadyExists, codes.InvalidArgument:
		return &models.Error{Code: badRequestCode, Message: status.Convert(err).Message()}
	default:
		return &models.Error{Code: internalCode, Message: internalFormat}
	}
}
