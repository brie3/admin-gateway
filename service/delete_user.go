package service

import (
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/users"
	"log"

	"github.com/go-openapi/runtime/middleware"
)

// DeleteUser handles user deletion logic.
func (g *Gateway) DeleteUser(params users.DeleteUserParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return users.NewDeleteUserUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return users.NewDeleteUserForbidden()
	}
	_, err := g.UserClient.DeleteUser(params.HTTPRequest.Context(), &pbUser.DeleteUserRequest{Id: params.UserID})
	if err != nil {
		log.Printf(deleteUserFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
		return users.NewDeleteUserDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	return users.NewDeleteUserNoContent()
}
