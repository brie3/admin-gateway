package service

import (
	"admin-gateway/models"
	pb "admin-gateway/proto/movie"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/movies"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetMovie handles get movie logic.
func (g *Gateway) GetMovie(params movies.GetMovieParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return movies.NewGetMovieUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return movies.NewGetMovieForbidden()
	}
	gm, err := g.MovieClient.GetMovie(params.HTTPRequest.Context(), &pb.MovieRequest{Id: params.MovieID})
	if err != nil {
		log.Printf(movieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
		return movies.NewGetMovieDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movie{}
	if err = swag.DynamicJSONToStruct(&gm, &mm); err != nil {
		log.Printf(jsonConvertOnGetMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewGetMovieOK().WithPayload(&mm)
}
