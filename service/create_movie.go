package service

import (
	"admin-gateway/models"
	pbMovie "admin-gateway/proto/movie"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/movies"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetMovie handles getmovie logic.
func (g *Gateway) CreateMovie(params movies.CreateMovieParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return movies.NewCreateMovieUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return movies.NewCreateMovieForbidden()
	}
	cm, err := g.MovieClient.CreateMovie(params.HTTPRequest.Context(), &pbMovie.CreateMovieRequest{
		Title:       params.Body.Title,
		Poster:      params.Body.Poster,
		MovieUrl:    params.Body.MovieURL,
		Content:     params.Body.Content,
		Duration:    params.Body.Duration,
		Director:    params.Body.Director,
		AgeRating:   pbMovie.AgeRating(params.Body.AgeRating),
		Visibility:  pbMovie.Visibility(params.Body.Visibility),
		Category:    pbMovie.Category(params.Body.Category),
		ReleaseDate: params.Body.ReleaseDate,
		OtherData:   params.Body.OtherData,
		IsPaid:      swag.BoolValue(params.Body.IsPaid),
	})
	if err != nil {
		log.Printf(createMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Title, err)
		return movies.NewCreateMovieDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movie{}
	if err = swag.DynamicJSONToStruct(&cm, &mm); err != nil {
		log.Printf(jsonConvertOnCreateMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Title, err)
		return movies.NewCreateMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewCreateMovieCreated().WithPayload(&mm)
}
