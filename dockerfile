FROM golang:1.15 as modules
ADD go.mod go.sum /m/
RUN cd /m && go mod download

FROM golangci/golangci-lint:v1.33-alpine
COPY --from=modules /go/pkg /go/pkg

ADD . /app
WORKDIR /app

RUN golangci-lint run --issues-exit-code=1 --deadline=600s .

FROM golang:1.15 as builder
COPY --from=modules /go/pkg /go/pkg

ADD . /app
WORKDIR /app

RUN useradd -u 10001 app_user
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o server .

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
USER app_user

COPY --from=builder /app/server /
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

EXPOSE 8081
CMD ["/server"]