// Code generated by go-swagger; DO NOT EDIT.

package movies

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// UpdateMovieHandlerFunc turns a function with the right signature into a update movie handler
type UpdateMovieHandlerFunc func(UpdateMovieParams, interface{}) middleware.Responder

// Handle executing the request and returning a response
func (fn UpdateMovieHandlerFunc) Handle(params UpdateMovieParams, principal interface{}) middleware.Responder {
	return fn(params, principal)
}

// UpdateMovieHandler interface for that can handle valid update movie params
type UpdateMovieHandler interface {
	Handle(UpdateMovieParams, interface{}) middleware.Responder
}

// NewUpdateMovie creates a new http.Handler for the update movie operation
func NewUpdateMovie(ctx *middleware.Context, handler UpdateMovieHandler) *UpdateMovie {
	return &UpdateMovie{Context: ctx, Handler: handler}
}

/*UpdateMovie swagger:route PUT /movies/{movie_id} movies updateMovie

UpdateMovie update movie API

*/
type UpdateMovie struct {
	Context *middleware.Context
	Handler UpdateMovieHandler
}

func (o *UpdateMovie) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewUpdateMovieParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal interface{}
	if uprinc != nil {
		principal = uprinc
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
