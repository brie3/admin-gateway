package service

import (
	"admin-gateway/models"
	pbAuth "admin-gateway/proto/auth"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/users"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// CreateUser handles create user logic.
func (g *Gateway) CreateUser(params users.CreateUserParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return users.NewCreateUserUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return users.NewCreateUserForbidden()
	}
	pr, err := g.AuthClient.PwdHash(params.HTTPRequest.Context(), &pbAuth.PwdRequest{Pwd: params.Body.Password})
	if err != nil {
		log.Printf(createPwdFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewCreateUserDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	cr, err := g.UserClient.CreateUser(params.HTTPRequest.Context(), &pbUser.CreateUserRequest{
		Email:       params.Body.Email.String(),
		PwdHash:     pr.GetPwdHash(),
		Role:        pbUser.UserRole(params.Body.Role),
		Status:      pbUser.UserStatus(params.Body.Status),
		DispayName:  params.Body.DisplayName,
		PhoneNumber: params.Body.PhoneNumber,
		BirthDate:   params.Body.BirthDate,
	})
	if err != nil {
		log.Printf(createUserFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewCreateUserDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mu := models.User{}
	if err = swag.DynamicJSONToStruct(&cr, &mu); err != nil {
		log.Printf(jsonConvertOnCreateUserFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Email.String(), err)
		return users.NewCreateUserDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return users.NewCreateUserCreated().WithPayload(&mu)
}
