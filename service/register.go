package service

import (
	"admin-gateway/models"
	pbAuth "admin-gateway/proto/auth"
	"admin-gateway/restapi/operations/auth"
	"log"

	"github.com/go-openapi/runtime/middleware"
)

// Register handles register logic.
func (g *Gateway) Register(params auth.RegisterUserParams) middleware.Responder {
	rr, err := g.AuthClient.Register(params.HTTPRequest.Context(), &pbAuth.RegisterRequest{Email: params.Body.Email.String(), Pwd: params.Body.Password})
	if err != nil {
		log.Printf(registerFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, params.Body.Email.String(), err)
		return auth.NewRegisterUserDefault(auth.RegisterUserBadRequestCode).WithPayload(wrapGRPCError(err))
	}
	return auth.NewRegisterUserOK().WithPayload(&models.RegisterResponse{Jwt: rr.Jwt})
}
