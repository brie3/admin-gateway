// Code generated by go-swagger; DO NOT EDIT.

package movies

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"admin-gateway/models"
)

// DeleteMovieNoContentCode is the HTTP code returned for type DeleteMovieNoContent
const DeleteMovieNoContentCode int = 204

/*DeleteMovieNoContent successful movie deletion

swagger:response deleteMovieNoContent
*/
type DeleteMovieNoContent struct {
}

// NewDeleteMovieNoContent creates DeleteMovieNoContent with default headers values
func NewDeleteMovieNoContent() *DeleteMovieNoContent {

	return &DeleteMovieNoContent{}
}

// WriteResponse to the client
func (o *DeleteMovieNoContent) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(204)
}

// DeleteMovieUnauthorizedCode is the HTTP code returned for type DeleteMovieUnauthorized
const DeleteMovieUnauthorizedCode int = 401

/*DeleteMovieUnauthorized unauthorized access for a lack of authentication

swagger:response deleteMovieUnauthorized
*/
type DeleteMovieUnauthorized struct {
}

// NewDeleteMovieUnauthorized creates DeleteMovieUnauthorized with default headers values
func NewDeleteMovieUnauthorized() *DeleteMovieUnauthorized {

	return &DeleteMovieUnauthorized{}
}

// WriteResponse to the client
func (o *DeleteMovieUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// DeleteMovieForbiddenCode is the HTTP code returned for type DeleteMovieForbidden
const DeleteMovieForbiddenCode int = 403

/*DeleteMovieForbidden forbidden access for a lack of sufficient privileges

swagger:response deleteMovieForbidden
*/
type DeleteMovieForbidden struct {
}

// NewDeleteMovieForbidden creates DeleteMovieForbidden with default headers values
func NewDeleteMovieForbidden() *DeleteMovieForbidden {

	return &DeleteMovieForbidden{}
}

// WriteResponse to the client
func (o *DeleteMovieForbidden) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(403)
}

/*DeleteMovieDefault other error response

swagger:response deleteMovieDefault
*/
type DeleteMovieDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewDeleteMovieDefault creates DeleteMovieDefault with default headers values
func NewDeleteMovieDefault(code int) *DeleteMovieDefault {
	if code <= 0 {
		code = 500
	}

	return &DeleteMovieDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the delete movie default response
func (o *DeleteMovieDefault) WithStatusCode(code int) *DeleteMovieDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the delete movie default response
func (o *DeleteMovieDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the delete movie default response
func (o *DeleteMovieDefault) WithPayload(payload *models.Error) *DeleteMovieDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the delete movie default response
func (o *DeleteMovieDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DeleteMovieDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
