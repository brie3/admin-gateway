package service

import (
	"admin-gateway/models"
	pbMovie "admin-gateway/proto/movie"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/movies"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// UpdateMovie handles update movie logic.
func (g *Gateway) UpdateMovie(params movies.UpdateMovieParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return movies.NewUpdateMovieUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return movies.NewUpdateMovieForbidden()
	}
	um, err := g.MovieClient.UpdateMovie(params.HTTPRequest.Context(), &pbMovie.UpdateMovieRequest{
		Id:          params.MovieID,
		Title:       params.Body.Title,
		Poster:      params.Body.Poster,
		MovieUrl:    params.Body.MovieURL,
		Content:     params.Body.Content,
		Duration:    params.Body.Duration,
		Director:    params.Body.Director,
		AgeRating:   pbMovie.AgeRating(params.Body.AgeRating),
		Visibility:  pbMovie.Visibility(params.Body.Visibility),
		Category:    pbMovie.Category(params.Body.Category),
		ReleaseDate: params.Body.ReleaseDate,
		OtherData:   params.Body.OtherData,
		IsPaid:      swag.BoolValue(params.Body.IsPaid),
	})
	if err != nil {
		log.Printf(updateMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Title, err)
		return movies.NewUpdateMovieDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movie{}
	if err = swag.DynamicJSONToStruct(&um, &mm); err != nil {
		log.Printf(jsonConvertOnUpdateMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, params.Body.Title, err)
		return movies.NewUpdateMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewUpdateMovieOK().WithPayload(&mm)
}
