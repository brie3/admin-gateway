package service

import (
	"admin-gateway/models"
	pb "admin-gateway/proto/movie"
	pbUser "admin-gateway/proto/user"
	"admin-gateway/restapi/operations/movies"
	"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetMovies handles getmovies logic.
func (g *Gateway) GetMovies(params movies.GetMoviesParams, principal interface{}) middleware.Responder {
	au, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr)
		return movies.NewGetMoviesUnauthorized()
	}
	if au.Role != int32(pbUser.UserRole_UserRole_ADMIN) {
		log.Printf(adminValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email)
		return movies.NewGetMoviesForbidden()
	}
	mc, err := g.MovieClient.GetMovies(params.HTTPRequest.Context(), &pb.MoviesRequest{
		Limit:           params.Limit,
		Offset:          swag.Int32Value(params.Offset),
		AgeRating:       pb.AgeRating(swag.Int32Value(params.AgeRating)),
		Visibility:      pb.Visibility(swag.Int32Value(params.Visibility)),
		Category:        pb.Category(swag.Int32Value(params.Category)),
		FromReleaseYear: swag.Int32Value(params.FromReleaseYear),
		ToReleaseYear:   swag.Int32Value(params.ToReleaseYear),
		UserId:          swag.Int32Value(params.UserID),
		OnlyPaid:        swag.BoolValue(params.OnlyRented),
	})
	if err != nil {
		log.Printf(moviesFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, au.Email, err)
		return movies.NewGetMoviesDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movies{}
	if err = swag.DynamicJSONToStruct(&mc, &mm); err != nil {
		log.Printf(jsonConvertOnGetMoviesFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMoviesDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewGetMoviesOK().WithPayload(&mm)
}
