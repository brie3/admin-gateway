// Code generated by go-swagger; DO NOT EDIT.

package movies

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"admin-gateway/models"
)

// CreateMovieCreatedCode is the HTTP code returned for type CreateMovieCreated
const CreateMovieCreatedCode int = 201

/*CreateMovieCreated single movie

swagger:response createMovieCreated
*/
type CreateMovieCreated struct {

	/*
	  In: Body
	*/
	Payload *models.Movie `json:"body,omitempty"`
}

// NewCreateMovieCreated creates CreateMovieCreated with default headers values
func NewCreateMovieCreated() *CreateMovieCreated {

	return &CreateMovieCreated{}
}

// WithPayload adds the payload to the create movie created response
func (o *CreateMovieCreated) WithPayload(payload *models.Movie) *CreateMovieCreated {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create movie created response
func (o *CreateMovieCreated) SetPayload(payload *models.Movie) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateMovieCreated) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(201)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// CreateMovieUnauthorizedCode is the HTTP code returned for type CreateMovieUnauthorized
const CreateMovieUnauthorizedCode int = 401

/*CreateMovieUnauthorized unauthorized access for a lack of authentication

swagger:response createMovieUnauthorized
*/
type CreateMovieUnauthorized struct {
}

// NewCreateMovieUnauthorized creates CreateMovieUnauthorized with default headers values
func NewCreateMovieUnauthorized() *CreateMovieUnauthorized {

	return &CreateMovieUnauthorized{}
}

// WriteResponse to the client
func (o *CreateMovieUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// CreateMovieForbiddenCode is the HTTP code returned for type CreateMovieForbidden
const CreateMovieForbiddenCode int = 403

/*CreateMovieForbidden forbidden access for a lack of sufficient privileges

swagger:response createMovieForbidden
*/
type CreateMovieForbidden struct {
}

// NewCreateMovieForbidden creates CreateMovieForbidden with default headers values
func NewCreateMovieForbidden() *CreateMovieForbidden {

	return &CreateMovieForbidden{}
}

// WriteResponse to the client
func (o *CreateMovieForbidden) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(403)
}

/*CreateMovieDefault other error response

swagger:response createMovieDefault
*/
type CreateMovieDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewCreateMovieDefault creates CreateMovieDefault with default headers values
func NewCreateMovieDefault(code int) *CreateMovieDefault {
	if code <= 0 {
		code = 500
	}

	return &CreateMovieDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the create movie default response
func (o *CreateMovieDefault) WithStatusCode(code int) *CreateMovieDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the create movie default response
func (o *CreateMovieDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the create movie default response
func (o *CreateMovieDefault) WithPayload(payload *models.Error) *CreateMovieDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create movie default response
func (o *CreateMovieDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateMovieDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
