// Code generated by go-swagger; DO NOT EDIT.

package movies

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"admin-gateway/models"
)

// GetMoviesOKCode is the HTTP code returned for type GetMoviesOK
const GetMoviesOKCode int = 200

/*GetMoviesOK multiple movies

swagger:response getMoviesOK
*/
type GetMoviesOK struct {

	/*
	  In: Body
	*/
	Payload *models.Movies `json:"body,omitempty"`
}

// NewGetMoviesOK creates GetMoviesOK with default headers values
func NewGetMoviesOK() *GetMoviesOK {

	return &GetMoviesOK{}
}

// WithPayload adds the payload to the get movies o k response
func (o *GetMoviesOK) WithPayload(payload *models.Movies) *GetMoviesOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get movies o k response
func (o *GetMoviesOK) SetPayload(payload *models.Movies) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetMoviesOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// GetMoviesUnauthorizedCode is the HTTP code returned for type GetMoviesUnauthorized
const GetMoviesUnauthorizedCode int = 401

/*GetMoviesUnauthorized unauthorized access for a lack of authentication

swagger:response getMoviesUnauthorized
*/
type GetMoviesUnauthorized struct {
}

// NewGetMoviesUnauthorized creates GetMoviesUnauthorized with default headers values
func NewGetMoviesUnauthorized() *GetMoviesUnauthorized {

	return &GetMoviesUnauthorized{}
}

// WriteResponse to the client
func (o *GetMoviesUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// GetMoviesForbiddenCode is the HTTP code returned for type GetMoviesForbidden
const GetMoviesForbiddenCode int = 403

/*GetMoviesForbidden forbidden access for a lack of sufficient privileges

swagger:response getMoviesForbidden
*/
type GetMoviesForbidden struct {
}

// NewGetMoviesForbidden creates GetMoviesForbidden with default headers values
func NewGetMoviesForbidden() *GetMoviesForbidden {

	return &GetMoviesForbidden{}
}

// WriteResponse to the client
func (o *GetMoviesForbidden) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(403)
}

/*GetMoviesDefault other error response

swagger:response getMoviesDefault
*/
type GetMoviesDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetMoviesDefault creates GetMoviesDefault with default headers values
func NewGetMoviesDefault(code int) *GetMoviesDefault {
	if code <= 0 {
		code = 500
	}

	return &GetMoviesDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the get movies default response
func (o *GetMoviesDefault) WithStatusCode(code int) *GetMoviesDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the get movies default response
func (o *GetMoviesDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the get movies default response
func (o *GetMoviesDefault) WithPayload(payload *models.Error) *GetMoviesDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get movies default response
func (o *GetMoviesDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetMoviesDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
