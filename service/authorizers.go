package service

import (
	"crypto/rsa"
	"errors"
	"io/ioutil"
	"log"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	verifyKey       *rsa.PublicKey
	errUnauthorized = errors.New("unauthorized")
)

// User stands for custom type for use within auth token.
type User struct {
	ID    int32
	Role  int32 // user role (1 - client; 2 - admin)
	Email string
}

// Claim stands for auth service claim.
type Claim struct {
	*jwt.StandardClaims
	User
}

func init() {
	publicKeyPath, ok := os.LookupEnv("AUTH_KEY_PATH")
	if !ok {
		log.Fatal("AUTH_KEY_PATH env is not set")
	}
	verifyBytes, err := ioutil.ReadFile(publicKeyPath)
	if err != nil {
		log.Fatalf("public key file is not set: %v", err)
	}
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		log.Fatalf("public key parsing failed: %v", err)
	}
}

// Authorize tells if the API key is a JWT signed by us with a claim to be a user.
func Authorize(token string) (*User, error) {
	return parseToken(token)
}

func parseToken(token string) (*User, error) {
	parsedToken, err := jwt.ParseWithClaims(token, &Claim{},
		func(parsedToken *jwt.Token) (interface{}, error) {
			return verifyKey, nil
		})
	if err != nil {
		return nil, errUnauthorized
	}
	return &parsedToken.Claims.(*Claim).User, nil
}
